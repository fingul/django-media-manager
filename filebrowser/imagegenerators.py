import os
from django.conf import settings
from imagekit import ImageSpec, register
from imagekit.exceptions import MissingSource
from pilkit.utils import open_image, img_to_fobj

IMAGEKIT_DEBUG = getattr(settings, 'IMAGEKIT_DEBUG', False)

class ImageCropProcessor(object):

    def process(self, image, cropspec):
        if cropspec == "0,0,0,0":
            return image
        else:
            spec = [int(i) for i in cropspec.split(",")]
            return image.crop(spec)

class ImageCropSpec(ImageSpec):
    processor = ImageCropProcessor()
    format = 'JPEG'
    options = {'quality': 90}

    def __init__(self, *args, **kwargs):
        self.cropspec = kwargs.pop('cropspec', '0,0,0,0')
        super().__init__(*args, **kwargs)

    @property
    def cachefile_name(self):
        fname = super().cachefile_name
        if fname:
            root, ext = os.path.splitext(fname)
            if ext:
                fname = "{0}_{1}{2}".format(root, self.cropspec.replace(",", ""), ext)
        return fname

    def generate(self):
        if not self.source:
            if IMAGEKIT_DEBUG:
                raise MissingSource("The spec '%s' has no source file associated"
                                    " with it." % self)
            else:
                print("ImageKit MissingSource: The spec '%s' has no source file "
                      "associated with it. Set `settings.IMAGEKIT_DEBUG = True` "
                      "to raise an exception and view stacktrace." % self)
                return None

        try:
            img = open_image(self.source)
        except ValueError:
            self.source.open()
            img = open_image(self.source)

        original_format = img.format

        # Run the processors
        img = self.processor.process(img, self.cropspec)

        options = self.options or {}
        return img_to_fobj(img, original_format, self.autoconvert, **options)

register.generator("filebrowser:cropped", ImageCropSpec)