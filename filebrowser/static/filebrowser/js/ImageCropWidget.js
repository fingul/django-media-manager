$(function () {

    $.widget("sixfoot.ImageCropFileBrowser", {

        options: {
            _jcrop_api: null,

            fbHref: null,
            mediaUrl: "/static/media",
            imageCropCoordField: null,
            imageCropArea: null,
            // This is the max width or height of the area that holds the cropper
            imageCropAreaDimensions: [500, 500],
            // Minimum size of the crop box
            imageCropDimensions: [1900, 430]
        },

        _create: function () {
            var _this = this;
            $('script').each(function (index, element) {
                var src = element.src;
                if (src.match(/ImageCropWidget/)) {
                    var idx = src.indexOf('filebrowser/js/ImageCropWidget');
                    _this.options["_admin_media_prefix"] = src.substring(0, idx);
                    // Stop iteration
                    return false;
                }
            });
            this._initCropArea();
        },

        show: function () {
            var _this = this,
                href = _this.options["fbHref"],
                id = _this.element.attr("id");

            var id2 = String(id).replace(/\-/g, "____").split(".").join("___");
            var fb_window = window.open(href, String(id2), 'height=600,width=960,resizable=yes,scrollbars=yes');
            fb_window.sendClose = false;
            fb_window.focus();

            fb_window.onunload = function () {
                var interval;
                interval = setInterval(function () {
                    if (fb_window.closed) {
                        clearInterval(interval);
                        _this._trigger("FBClosed");
                        _this._updateCropArea();
                    }
                }, 100);
            }
        },

        clear: function (id) {
            var _this = this;
            if (confirm('Are you sure you want to clear this image? It will not delete the file.')) {
                this.element.val("");
            }
        },

        _initCropArea: function () {
            var _this = this;

            var area_dimensions = _this.options.imageCropAreaDimensions;
            var crop_dimensions = _this.options.imageCropDimensions;
            var JCROP_OPTIONS = {
                aspectRatio: crop_dimensions[0] / crop_dimensions[1],
                minSize: crop_dimensions,
                maxSize: [0, 0],
                boxWidth: area_dimensions[0],
                boxHeight: area_dimensions[1],

                onSelect: function (e) {
                    var x = parseInt(e.x),
                        y = parseInt(e.y),
                        x2 = parseInt(e.x2),
                        y2 = parseInt(e.y2);
                    _this.options.imageCropCoordField.val(x + "," + y + "," + x2 + "," + y2);
                },

                onRelease: function () {
                    _this.options.imageCropCoordField.val("0,0,0,0");
                }
            };

            if (_this.options._jcrop_api === null) {
                _this.options.imageCropArea.Jcrop(JCROP_OPTIONS, function () {
                    _this.options._jcrop_api = this;
                    var raw_coords = _this.options.imageCropCoordField.val();
                    if (raw_coords !== '0,0,0,0') {
                        var coords = raw_coords.split(',');
                        _this.options._jcrop_api.setSelect(coords);
                    }
                });
            }
        },

        _updateCropArea: function () {
            var _this = this;
            var imgUrl = _this.options.mediaUrl + _this.element.val();
            var crop_dimensions = _this.options.imageCropDimensions;

            // Verify that the image is at least MIN_WIDTHxMIN_HEIGHT
            // using banana for scale
            var banana = new Image();
            banana.onload = function () {
                if (banana.width < crop_dimensions[0] || banana.height < crop_dimensions[1]) {
                    alert("Image is smaller than minimum required resolution.");
                    _this.element.val("");
                } else {
                    // If the image is large enough, load it into the crop area
                    _this.options.imageCropArea.attr('src', imgUrl);
                    if (_this.options._jcrop_api === null) {
                        _this._initCropArea();
                    }
                    _this.options._jcrop_api.setImage(imgUrl);
                }
            };
            banana.src = imgUrl;
        }
    });
});
