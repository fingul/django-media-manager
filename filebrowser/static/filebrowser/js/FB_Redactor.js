/**
 *
 *    $$\      $$\$$$$$$$$\$$$$$$$\  $$$$$$\ $$\   $$\$$$$$$$\ $$\      $$$$$$\$$\      $$\$$$$$$$$\
 *    $$ | $\  $$ $$  _____$$  __$$\$$  __$$\$$ |  $$ $$  __$$\$$ |     \_$$  _$$$\    $$$ $$  _____|
 *    $$ |$$$\ $$ $$ |     $$ |  $$ $$ /  \__$$ |  $$ $$ |  $$ $$ |       $$ | $$$$\  $$$$ $$ |
 *    $$ $$ $$\$$ $$$$$\   $$$$$$$\ \$$$$$$\ $$ |  $$ $$$$$$$\ $$ |       $$ | $$\$$\$$ $$ $$$$$\
 *    $$$$  _$$$$ $$  __|  $$  __$$\ \____$$\$$ |  $$ $$  __$$\$$ |       $$ | $$ \$$$  $$ $$  __|
 *    $$$  / \$$$ $$ |     $$ |  $$ $$\   $$ $$ |  $$ $$ |  $$ $$ |       $$ | $$ |\$  /$$ $$ |
 *    $$  /   \$$ $$$$$$$$\$$$$$$$  \$$$$$$  \$$$$$$  $$$$$$$  $$$$$$$$\$$$$$$\$$ | \_/ $$ $$$$$$$$\
 *    \__/     \__\________\_______/ \______/ \______/\_______/\________\______\__|     \__\________|
 *
 *
 *    -----------------------------------------------------------------------------------------------
 *
 *    @author: websublime.com
 *    @version: 1.0
 *    @description: Redactor suit editor.
 */
// if (typeof RedactorPlugins === 'undefined') var RedactorPlugins = {};
// if (typeof RedactorObject === 'undefined') var RedactorObject = {};
window.RedactorPlugins = window.RedactorPlugins || {};
window.RedactorObject = window.RedactorObject || {};
(function($){
RedactorPlugins.filebrowser = function() {return{
    init: function() {
        // v10
        if (this.button) {
            var button = this.button.add('browser', 'File Browser');
            this.button.setAwesome('browser', 'fa-folder');
            this.button.addCallback(button, this.filebrowser.activeBrowser.bind(this));
        } else { // v9
            this.addBtn('browser', 'File Browser', this.activeBrowser.bind(this));
        }
    },
    activeBrowser: function() {
        var win = window.open(
            '/admin/filebrowser/browse?pop=4&Redactor='+this.$element.attr('id'),
            'File Browser',
            'height=600,width=960,resizable=yes,scrollbars=yes'
        );

        RedactorObject = this;
    }
};};

if(window.opener && $){
    window.onload = function() {
        $('button[name="redactor-select"]').on('click', $.proxy(function(e){

            var target = $(e.target);
            var tag;

            if(target.data('type')){
                if(target.data('type') == 'Image'){
                    tag = '<img src="'+target.data('file')+'">';
                    // v10
                    if (this.insertHtml)
                        this.insertHtml(tag);
                    else // v8
                        this.insert.html(tag);
                    window.close();
                } else if(target.data('type') == 'Document'){
                    tag = '<a href="'+target.data('File')+'">'+target.data('type')+'</a>';
                    // v10
                    if (this.insertHtml)
                        this.insertHtml(tag);
                    else // v8
                        this.insert.html(tag);
                    window.close();
                }
            }
        }, window.opener.RedactorObject));

    };
}

function GetURLParameter(sParam){
    console.log(window.location);
    var sPageURL = window.location.search.substring(1);

    var sURLVariables = sPageURL.split('&');

    var parameter;

    for (var i = 0; i < sURLVariables.length; i++){
        var sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] == sParam){
            parameter = sParameterName[1];
            break;
        }
    }

    return parameter;
}
})(jQuery);
